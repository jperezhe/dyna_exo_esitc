#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# DYNA_NON_LINE - Résolution dynamique transitoire d'un système non-linéaire
# avec un schéma temporel de Newmark couplé à un schéma de résolution de
# Newton-Raphson (ou Newton-Raphson modifié)
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   12/12/2021
#-----------------------------------------------------------------------------#
import collections
import numpy
from numpy import array, zeros, dot
# from numpy.linalg import inv
from scipy.linalg import inv        # faster than numpy
from math import sin, cos, pi, sqrt

from pyfem.beam import beam
from pyfem.mass import mass
from pyfem.spring import spring
from pyfem.dashpot import dashpot
from pyfem.springNL import springNL
from pyfem.springNLgliss import springNLgliss


def matrix_assembly(Ma,Mb,lai,laj=None):
    """
    Assembly function of two matrix. Ma matrix is supposed to be
    a large matrix where we need to include values from Mb matrix.
    'la' is the list of index of destination cells in Ma matrix (they
    need to be in the same order that was used to get Mb matrix!)
    """
    if not laj:
        Ma[numpy.ix_(lai,lai)] += Mb
    else:
        Ma[numpy.ix_(lai,laj)] += Mb
    return Ma


def DYNA_NON_LINE(MODEL,TIME,ACC,TOL=1e-9,SCHEME="mNR",NITER_MAX=20,INFO=False):
    """
    Algorithme de résolution dynamique transitoire d'un système non-linéaire
    avec un schéma temporel de Newmark couplé à un schéma de résolution de
    Newton-Raphson (ou de Newton-Raphson modifié)
    - in:
        MODEL       - dictionnaire avec les informations du modèle de calcul:
                        n_dof                   - nombre de degrés de liberté dans le problème
                        l_elem                  - liste qui contient l'information des éléments
                                                  finis utilisés dans la définition du modèle
                        l_dof_bloq              - liste de ddl bloqués
                        l_dof_free              - liste des ddl libres
                        l_dof_earthquake_load   - liste des ddl auquel s'applique
                                                  le vecteur de chargement équivalent
        TIME        - vecteur avec les instants de temps de l'accélérogramme
        ACC         - accélérogramme de calcul
        TOL         - tolerance à utiliser dans le critère de convergence
                      de l'algorithme de résolution non-linéaire (par
                      défaut = 1e-9)
        SCHEME      - schema de résolution problème non-linéaire
                        = "mNR" - modified Newton-Raphson (par défaut)
                        = "NR"  - Newton-Raphson
        NITER_MAX   - nombre d'itérations maximale à effectuer dans les
                      schemas de résolution non-linéaires (= 20 par défaut)
        INFO        - if true, script prints information about resolution
                      (= False par défaut)
    - out:
        RES         - dictionnaire avec les résultats du calcul
    """
    
    n_dof = MODEL["n_dof"]
    l_elem = MODEL["l_elem"]
    l_dof_bloq = MODEL["l_dof_bloq"]
    l_dof_free = MODEL["l_dof_free"]
    l_dof_earthquake_load = MODEL["l_dof_earthquake_load"]
    
    # pas de temps
    # Important: il faut que le pas de temps de l'accélérogramme soit constant
    DT = TIME[1]-TIME[0]
    n_step = len(TIME)-1
    
    # paramètres schéma d'intégration de Newmark
    gamma_NK = 1./2.
    beta_NK  = 1./4.
    
    
    # initialisation des matrices et des vecteurs globaux
    K = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice de raideur
    C = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice d'amortissement
    M = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice de masse
    F = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des forces
    U = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des déplacements
    V = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des vitesses
    A = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des accélérations
    
    
    # Assemblage de la matrice de raideur
    for elem,ldof in l_elem:
        if isinstance(elem,(spring,springNL,beam,springNLgliss)):
            K = matrix_assembly(K, elem.K, ldof)
    
    # Assemblage de la matrice de masse
    for elem,ldof in l_elem:
        if isinstance(elem,(mass)):
            M = matrix_assembly(M, elem.M, ldof)
    
    # Assemblage de la matrice d'amortissement
    for elem,ldof in l_elem:
        if isinstance(elem,(dashpot)):
            C = matrix_assembly(C, elem.C, ldof)
    
    
    # Force equivalente pour un chargement sismique
    delta = numpy.zeros(n_dof, dtype=numpy.float64)
    for dof in l_dof_earthquake_load:
        delta[dof] = 1.
    # on reactualise le vecteur de chargement avec la charge équivalente
    # (en realité F est une matrice qui contient les différents vecteurs
    # de chargement pour chaque instant du temps)
    F += -1.*numpy.outer(dot(M,delta),ACC)
    
    
    
    # Décomposition de la matrice de raideur en sous-matrices pour la
    # résolution du système en deux temps (d'abord sur les ddl libres
    # pour calculer leur déplacement et ensuite sur les ddl fixes pour
    # obtenir les réactions aux appuis
    K11 = K[numpy.ix_(l_dof_free,l_dof_free)]
    K12 = K[numpy.ix_(l_dof_free,l_dof_bloq)]
    K21 = numpy.transpose(K12)
    K22 = K[numpy.ix_(l_dof_bloq,l_dof_bloq)]
    
    # Décomposition de la matrice d'amortissement en sous-matrices pour la
    # résolution du système en deux temps (d'abord sur les ddl libres
    # pour calculer leur déplacement et ensuite sur les ddl fixes pour
    # obtenir les réactions aux appuis
    C11 = C[numpy.ix_(l_dof_free,l_dof_free)]
    C12 = C[numpy.ix_(l_dof_free,l_dof_bloq)]
    C21 = numpy.transpose(C12)
    C22 = C[numpy.ix_(l_dof_bloq,l_dof_bloq)]
    
    # Décomposition de la matrice de masse en sous-matrices pour la
    # résolution du système en deux temps (d'abord sur les ddl libres
    # pour calculer leur déplacement et ensuite sur les ddl fixes pour
    # obtenir les réactions aux appuis
    M11 = M[numpy.ix_(l_dof_free,l_dof_free)]
    M12 = M[numpy.ix_(l_dof_free,l_dof_bloq)]
    M21 = numpy.transpose(M12)
    M22 = M[numpy.ix_(l_dof_bloq,l_dof_bloq)]
    
    # Vecteur avec avec le chargement imposé aux ddl libres
    F1 = F[l_dof_free]
    
    # Vecteurs de déplacement, vitesse et accélération aux ddl libres
    U1 = U[l_dof_free]
    V1 = V[l_dof_free]
    A1 = A[l_dof_free]
    
    # vecteur des forces internes
    fSj = numpy.zeros(len(l_dof_free),dtype=numpy.float64)
    
    # calcul des constantes pour schéma de Newmark
    NK_a = (gamma_NK/beta_NK)*C11 + (1./(beta_NK*DT))*M11
    NK_b = DT*(gamma_NK/(2.*beta_NK)-1.)*C11 + (1./(2.*beta_NK))*M11
    
    for i in range(0,n_step):
        
        # calcul de l'incrément de force
        Dp = F1[:,i+1]-F1[:,i]
        Dpeq = Dp + dot(NK_a,V1[:,i]) + dot(NK_b,A1[:,i])
        
        # calcul de la matrice de raideur apparente
        Keq = K11 + (gamma_NK/(beta_NK*DT))*C11 + (1./(beta_NK*DT**2))*M11
        Keq_inv = inv(Keq)
        
        # Newton-Raphson modifié
        j = 0
        R = Dpeq
        U[:,i+1] = U[:,i]
        DU1 = numpy.zeros(len(l_dof_free),dtype=numpy.float64)
        R0 = R
        while not (sqrt(dot(R.T,R)) < TOL) or (j == 0):
            
            # calcul de l'incrément de déplacement
            DU1j = dot(Keq_inv,R)
            
            # mise à jour du déplacement à la fin du step (attention on travaille directement
            # avec le vecteur pour tout le système pas seulement les ddl libres)
            U[l_dof_free,i+1] += DU1j
            
            # itération sur les éléments pour calcule le vecteur de
            # forces internes et la nouvelle matrice de raideur tangente
            fSj1_all = numpy.zeros(n_dof,dtype=numpy.float64)
            K_ = numpy.zeros((n_dof,n_dof), dtype=numpy.float64)   # matrice de raideur
            for elem,ldof in l_elem:
                if isinstance(elem,(spring,springNL,beam,springNLgliss)):
                    f_int_elem,Ktan_elem = elem.iteration(U=U[ldof,i+1],n_iter=j)
                    fSj1_all[ldof] += f_int_elem
                    K_ = matrix_assembly(K_, Ktan_elem, ldof)
            fSj1 = fSj1_all[l_dof_free]
            
            # calcul de l'incrément de force réel
            Dfj = fSj1 - fSj + dot((Keq-K11),DU1j)
            
            # Mise à jour des variables avant vérification de la convergence
            R = R - Dfj
            fSj = fSj1
            DU1 += DU1j
            
            if SCHEME == "NR":
                # on utilise le schéma d'intégration Newton-Raphson classique
                K = K_
                K11 = K[numpy.ix_(l_dof_free,l_dof_free)]
                Keq = K11 + (gamma_NK/(beta_NK*DT))*C11 + (1./(beta_NK*DT**2))*M11
                Keq_inv = inv(Keq)
            
            if INFO:
                print("pas {} - iteration {} - R = {:.5e}".format(i,j,sqrt(dot(R.T,R))))
            j += 1
            
            if j > NITER_MAX:
                break
        
        # nouvelle matrice de raideur tangente à utiliser dans le prochain step
        K = K_
        K11 = K[numpy.ix_(l_dof_free,l_dof_free)]
        
        # calcul de l'incrément de vitesse et d'accélération
        DV1 = (gamma_NK/(beta_NK*DT))*DU1 - (gamma_NK/beta_NK)*V1[:,i] + \
               DT*(1.-(gamma_NK/(2.*beta_NK)))*A1[:,i]
        DA1 = (1./(beta_NK*DT**2))*DU1 - (1./(beta_NK*DT))*V1[:,i] - (1./(2.*beta_NK))*A1[:,i]
        
        # mise à jour des variables
        U1[:,i+1] = U1[:,i] + DU1
        V1[:,i+1] = V1[:,i] + DV1
        A1[:,i+1] = A1[:,i] + DA1
    
    U[l_dof_free] = U1
    V[l_dof_free] = V1
    A[l_dof_free] = A1
    
    
    # dictionnaire avec les résultats du calcul
    RES ={
        "U":     U,
        "V":     V,
        "A":     A,
        "TIME":  TIME,
        "MODEL": MODEL,
    }
    
    return RES


# test
if __name__ == '__main__':
    
    import xlrd
    import easy_excel
    
    # lecture accélérogramme
    book = xlrd.open_workbook("accélérogrammes.xls")
    sheet = book.sheet_by_name("Accélérogrammes")
    time,acc = easy_excel.read_interval(sheet,"C3:D10000")
    time = numpy.array(time)
    acc = numpy.array(acc)*9.81         # to convert to m/s2 units
    
    
    # définition du modèle de calcul avec tous les éléments linéaires élastiques
    # sauf un ressort à comportement élastique non-linéaire
    model_NL ={
        # nombre de degrés de liberté dans le problème
        "n_dof":3,
        
        # liste qui contient l'information des éléments finis utilisés
        # pour définir le modèle + initialisation des éléments
        "l_elem":[
            # elem / ldofs
            [springNL(k0=2000,du=0.005),[0,1]],
            [dashpot(c=4.5),[0,1]],
            [mass(m=1),[1]],
            [spring(k=1500),[1,2]],
            [dashpot(c=4.75),[1,2]],
            [mass(m=1.5),[2]],
        ],
        
        # liste de ddl bloqués
        "l_dof_bloq": [0],
        
        # liste des ddl libres
        "l_dof_free": [1,2],
        
        # liste des ddl auquel s'applique le vecteur de chargement équivalent
        "l_dof_earthquake_load": [0,1,2],
    }
    
    # calcul
    res_NL = DYNA_NON_LINE(model_NL,time,acc,INFO=True)


