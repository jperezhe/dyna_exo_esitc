#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Element discret de type masse ponctuelle
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   12/12/2021
#-----------------------------------------------------------------------------#
import numpy

class mass():
    
    def __init__(self,m=0.,**kwargs):
        """
        Initialisation d'un élémént discret de masse ponctuelle
        - in:
            m       - valeur de la masse ponctuelle
        """
        self.m = m
        
        # calcul de la matrice de masse de l'élément discret
        self.M = self.get_M_elem()
    
    def get_M_elem(self):
        """
        Calcul de la matrice de masse de l'élément
        """
        m = self.m
        return numpy.array([[m]])

# test
if __name__ == '__main__':
    
    print("----m1----")
    m1 = mass(m=1.)
    print(m1.M)
    
