#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Element discret de type amortisseur visqueux
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   12/12/2021
#-----------------------------------------------------------------------------#
import numpy
from numpy import dot

class dashpot():
    
    def __init__(self,c=0.,**kwargs):
        """
        Elément discret de type amortisseur visqueux
        - in:
            c        - coefficient d'amortissement
        """
        self.c  = c
        
        # calcul de la matrice d'amortissement de l'élément discret
        self.C = self.get_C_elem()
    
    def get_C_elem(self):
        """
        Calcul de la matrice d'amortissement de l'élément
        """
        c  = self.c
        return numpy.array([
                    [ c, -c],
                    [-c,  c]])

# test
if __name__ == '__main__':
    
    print("----c1----")
    c1 = dashpot(c=1.)
    print(c1.C)
    

