#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Element discret de type ressort élastique non-linéaire
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   12/12/2021
#-----------------------------------------------------------------------------#
import numpy
from numpy import dot

class springNL():
    
    def __init__(self,k0=None,du=None,**kwargs):
        """
        Elément discret de type ressort élastique non-linéaire
        - in:
            k0      - raideur initiale
            du      - déplacement (ou rotation) à la limite du domaine
                      élastique linéaire
        """
        
        self.k0 = k0
        self.du = du
        
        # variables internes
        self.state_values ={
            "ktan":k0,
            "U":numpy.zeros(2,dtype=numpy.float64),
            "F":numpy.zeros(2,dtype=numpy.float64),
        }
        
        # calcul de la matrice de raideur de l'élément discret
        self.K = self.get_K_elem()
    
    
    def get_K_elem(self):
        """
        Calcul de la matrice de raideur tangente de l'élément
        """
        ktan = self.state_values['ktan']
        return numpy.array([[ ktan,-ktan],
                            [-ktan, ktan]])
    
    def get_ktan(self,d):
        """
        Fonction qui donne à chaque valeur du déplacement rélatif
        entre les points en extrémité de l'élément la valeur de
        la raideur tangente correspondante. Cela correspond à une
        loi de comportement élastique non-linéaire 1D
        """
        if abs(d/self.du) <= 1.:
            return self.k0
        else:
            return self.k0*abs(self.du/d)**(3/2)
    
    def get_f_int(self,U=None):
        """
        Calcul de la force interne developée dans l'élément par le
        déplacements des noeuds en extremité et mise à jour de la
        raideur tangente
        """
        B = numpy.array([ -1., 1.])
        
        U0 = self.state_values['U']
        DU = U-U0
        d0 = dot(B,U0)
        dd = dot(B,DU)
        d1 = dot(B,U)
        fi = self.state_values["F"][1]
        
        # itérer pour calculer le vecteur de force interne
        niter = 100
        ddi = dd/niter
        for i in range(niter):
            di = d0+i*ddi
            ktan = self.get_ktan(di)
            dfi = ktan*ddi
            fi += dfi
        ktan = self.get_ktan(d1)
        
        # vecteur de forces internes et mise à jour de la raideur tangente
        f_int = B*fi
        self.state_values['ktan'] = ktan
        
        return f_int
    
    def iteration(self,U=None,**kwargs):
        """
        Résolution pour l'itération en cours
        - in:
            U       - vecteur de déplacements aux noeuds de l'élément
        - out:
            f_int   - vecteur de forces internes à l'élément
            K       - matrice de raideur tangente
        """
        
        # calcul du vecteur des forces internes et mise à jour de la
        # matrice de raideur tangente de l'élément
        f_int = self.get_f_int(U=U)
        self.K = self.get_K_elem()
        
        # enregistrement des variables internes
        self.state_values['U'] = U
        self.state_values['F'] = f_int
        
        return f_int, self.K

# test
if __name__ == '__main__':
    
    # test pour vérifier la loi de comportement
    print("----k1----")
    k1 = springNL(k0=10.,du=1)
    print(k1.K)
    
    print(k1.iteration(U=numpy.array([0,1])))
    print(k1.iteration(U=numpy.array([0,10])))
    
    
    print("----k2----")
    k2 = springNL(k0=10.,du=1)
    print(k2.K)
    
    print(k2.iteration(U=numpy.array([0,-1])))
    print(k2.iteration(U=numpy.array([0,-10])))
    
    