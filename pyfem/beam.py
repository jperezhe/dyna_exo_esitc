#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Elément de poutre droite de type Euler-Bernouilli. La section est constante
# sur la longueur. Le matériau est homogène, isotrope, élastique linéaire.
#
# Dans cette élément de poutre les hypothèses retenues sont les suivantes:
#   * hypothèse de Navier-Bernouilli: la section droite reste normale à la
#     la déformée de la fibre moyenne.
#   * hypothèse de Saint-Venant: la torsion est libre.
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   12/12/2021
#-----------------------------------------------------------------------------#
import numpy
from numpy import dot

class beam():
    
    def __init__(self,L=None,EI=None,**kwargs):
        """
        Initialisation d'un élément de poutre de type Euler-Bernouilli
        - in:
            L       - longueur de la poutre
            EI      - rigidité en flexion
        """
        self.L = L
        self.EI = EI
        
        # calcul de la matrice de raideur de l'élément
        self.K = self.get_K_elem()
    
    def get_K_elem(self):
        """
        Calcul de la matrice de raideur de l'élément
        """
        
        EI = self.EI
        L  = self.L
        
        kb = 12.*EI/L**3
        kc = 6.*EI/L**2
        ki = 4.*EI/L
        kj = 2.*EI/L
        
        return numpy.array([[ kb, kc,-kb, kc],
                            [ kc, ki,-kc, kj],
                            [-kb,-kc, kb,-kc],
                            [ kc, kj,-kc, ki]])
    
    def get_f_int(self,U=None):
        """
        Calcul de la force interne developée dans l'élément par le
        déplacements des noeuds en extremité
        - in:
            U   - vecteur des déplacements aux noeuds de l'élément
        """
        return dot(self.K,U)
    
    def iteration(self,U=None,**kwargs):
        """
        Fait la résolution pour l'itération en cours
        - in:
            U   - vecteur de déplacements aux noeuds de l'élément
        - out:
            f_int   - vecteur de forces internes à l'élément
            K       - matrice de raideur
        """
        
        # calcul des forces internes à l'élément
        f_int = self.get_f_int(U=U)
        
        return f_int, self.K


# test
if __name__ == '__main__':
    
    b1 = beam(L=1,EI=2)
    
    print(b1.K)
    

