#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Element discret de type ressort linéaire élastique
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   12/12/2021
#-----------------------------------------------------------------------------#
import numpy
from numpy import dot

class spring():
    
    def __init__(self,k=0.,**kwargs):
        """
        Elément discret de type ressort linéaire élastique
        - in:
            k       - raideur de l'élément
        """
        self.k  = k
        
        # calcul de la matrice de raideur de l'élément discret
        self.K = self.get_K_elem()
    
    def get_K_elem(self):
        """
        Calcul de la matrice de raideur de l'élément
        """
        k  = self.k
        return numpy.array([[ k,-k],
                            [-k, k]])
    
    def get_f_int(self,U_global=None):
        """
        Calcul de la force interne developée dans l'élément par le
        déplacements des noeuds en extremité
        """
        return dot(self.K,U_global)
    
    def iteration(self,U=None,**kwargs):
        """
        Résolution pour l'itération en cours
        - in:
            U       - vecteur de déplacements aux noeuds de l'élément
        - out:
            f_int   - vecteur de forces internes à l'élément
            K       - matrice de raideur
        """
        
        # calcul des forces internes à l'élément
        f_int = self.get_f_int(U_global=U)
        
        return f_int, self.K

# test
if __name__ == '__main__':
    
    print("----k1----")
    k1 = spring(k=1)
    print(k1.K)
    
    print("----f_int----")
    f_int,K = k1.iteration(U=[0,1])
    print(f_int)
    print(K)
    

