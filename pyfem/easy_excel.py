#!/bin/bash
# -*- coding: utf-8 -*-
"""
Snippet for easier handling of reading/writing of data from an Excel file
Author: J. Pérez <jesus.perez@setec.com>
Date:   11/05/2021
"""

from xlwt import XFStyle

def excel_style(row, col):
    """
    Convert given row and column number to an Excel-style cell name.
    """
    quot, rem = divmod(col-1, 26)
    return((chr(quot-1 + ord('A')) if quot else '') +
           (chr(rem + ord('A')) + str(row)))

def get_numerical_column(col):
    """
    Converts given Excel-style column to column number.
    """
    num = 0
    if len(col) == 1:
        num = ord(col) - 65
    else:
        num = (ord(col[0])-64)*26 + (ord(col[1])-65)
    return num

def get_cell(cell,sheet):
    """
    Converts given Excel-style cell name to row and column number.
    """
    if len(cell) < 1:
        raise ValueError('Error, the cell you entered is not valid')
    col = ''
    row = ''
    for i in range(0,len(cell)):
        if cell[i].isdigit() == False:
            col = col + cell[i]
        else:
            row = row + cell[i]
    if row == "":
        row = sheet.nrows
    row = int(row) - 1
    col = min(get_numerical_column(col.upper()), sheet.ncols-1)
    return row,col

def get_excel_range(row,col):
    """
    Return an Excel type range from a given row and column range
    - in:
        row         - string with selected lines, e.g:
                        '5:10'  -> lines 5 to 10
                        '7'     -> line 7
        col         - string with selected columns e.g:
                        'A:C'   -> columns A to C
                        'A'     -> column A
    - out:
        range       - string with range in excel format:
                        'A5:C10'
                        'A7'
    """
    row = row.split(':')
    col = col.split(':')
    range = None
    if (len(row) == 1) & (len(col) == 1):
        range = col[0] + row[0]
    elif (len(row) == 2) & (len(col) == 1):
        range = col[0] + row[0] + ':' + col[0] + row[1]
    elif (len(row) == 1) & (len(col) == 2):
        range = col[0] + row[0] + ':' + col[1] + row[0]
    elif (len(row) == 2) & (len(col) == 2):
        range = col[0] + row[0] + ':' + col[1] + row[1]
    else:
        print("Error (_get_excel_range): input data is not correct!")
    
    return range

def get_interval(interval,sheet,format=None):
    """
    Converts given Excel-style cell interval to a pair of tuples containing
    initial and final cell row and colum number.
    """
    if ':' in interval:
        icell,fcell = interval.split(':')
    elif format == "cell":
        icell = interval
        fcell = interval
    else:
        raise ValueError('Error, the interval you entered is not valid')
    icell = get_cell(icell,sheet)
    fcell = get_cell(fcell,sheet)
    return icell,fcell

def read_interval(sheet,interval,format="column",clean_empty=True,clean_only_tail=True):
    """
    Reads given Excel-style cell interval to several lists
        format = column     - one sublist per per column (default)
                 row        - one sublist per row
        clean_empty         - deletes elements with no value (empty cells in Excel)
                              from lists
        clean_only_tail    - by default only empty elements at the tail of the list
                             are deleted
    """
    icell,fcell = get_interval(interval,sheet)
    
    data = []
    if format=="column":
        for j in range(icell[1],fcell[1]+1):
            data.append(sheet.col_values(j,icell[0],fcell[0]+1))
    elif format=="row":
        for i in range(icell[0],fcell[0]+1):
            data.append(sheet.row_values(i,icell[1],fcell[1]+1))
    else:
        data = None
    
    # Deletes elements with no value (empty cells in Excel) from lists
    if clean_empty:
        if data:
            for i,l in enumerate(data):
                if clean_only_tail:
                    aux = l[::-1]
                    tail = True
                    for j,v in enumerate(l[::-1]):
                        if tail and (v != ""):
                            tail = False
                            aux = aux[j:]
                    if all(v == "" for v in aux):
                        data[i] = []
                    else:
                        data[i] = aux[::-1]
                else:
                    data[i] = [v for v in l if v != ""]
    return data

def read_cell(sheet,interval):
    """
    Reads value from given Excel-style cell
    """
    icell,fcell = get_interval(interval,sheet,format="cell")
    return sheet.col_values(icell[1],icell[0],fcell[0]+1)[0]

def read_all(sheet):
    """
    Reads a given sheet and returns a list with all the data
    """
    data = []
    for i in range(sheet.nrows):
        data.append(sheet.row_values(i))
    return data

def write_all(sheet,data,flist=None):
    """
    Writes a given list of data in sheet
    """
    if not flist:
        for i,line in enumerate(data):
            for j,v in enumerate(line):
                sheet.write(i,j,v)
    else:
        ds = easy_format(sheet,flist)
        for i,line in enumerate(data):
            for j,v in enumerate(line):
                s = ds.get(f"{i},{j}")
                if s:
                    sheet.write(i,j,v,s)
                else:
                    sheet.write(i,j,v)

def easy_format(sheet,flist):
    """
    Returns a dictionnary with the style to be applied to the
    formating of numerical values
    """
    ds = {}
    for df in flist:
        icol = get_numerical_column(df["icol"])
        ncol = df["ncol"]
        irow = df["irow"] - 1
        nrow = df["nrow"]
        fmt  = df["fmt"]
        
        style = XFStyle()
        style.num_format_str = fmt
        
        for i in range(irow,irow+nrow):
            for j in range(icol,icol+ncol):
                ds[f"{i},{j}"] = style
    return ds
